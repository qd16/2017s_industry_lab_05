package ictgradschool.industry.lab05.ex02;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // Populate the animals array with a Bird, a Dog and a Horse.
        animals[0] = new Dog();
        animals[1] = new Bird();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // Loop through all the animals in the given list, and print their details as shown in the lab handout.
        // If the animal also implements IFamous, print out that corresponding info too.
        for (IAnimal animal: list) {
            System.out.println(String.format("%s the %s says %s.", animal.myName(), animal.getClass().getSimpleName().toLowerCase(), animal.sayHello()));
            System.out.println(String.format("%s the %s is a %s.", animal.myName(), animal.getClass().getSimpleName().toLowerCase(), animal.isMammal()? "mammal" : "non-mammal"));
            System.out.println(String.format("Did I forget to tell you that I have %d legs.", animal.legCount()));
            if (animal instanceof IFamous){
                System.out.println("This is a famous name of my animal type: " + ((IFamous) animal).famous());
            }
            System.out.println("--------------------------------------------------------------");
        }
    }

    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
