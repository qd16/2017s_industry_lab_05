package ictgradschool.industry.lab05.ex01;

import javax.swing.text.Document;

public interface Scanner {
    public Document getDocument();
    public boolean jobsDone();
    public Error getError();
}
