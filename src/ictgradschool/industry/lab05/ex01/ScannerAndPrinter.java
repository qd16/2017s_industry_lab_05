package ictgradschool.industry.lab05.ex01;

import javax.swing.text.Document;


public class ScannerAndPrinter implements Scanner , Printer {
    @Override
    public Document getDocument() {
        return null;
    }

    @Override
    public boolean jobsDone() {
        return false;
    }

    @Override
    public void printDocument(Document d) {

    }

    @Override
    public int getEstimateMinutesRemaining() {
        return 0;
    }

    @Override
    public Error getError() {
        return null;
    }
}