package ictgradschool.industry.lab05.ex01;

import javax.swing.text.Document;

public interface Printer {
    public void printDocument (Document d);
    public int getEstimateMinutesRemaining();
    public Error getError();
}
