package ictgradschool.industry.lab05.quiz01;

public class RangedClass extends GameClass {
    public RangedClass(String name) {
        super(name);
    }

    @Override
    public void attack(GameClass target) {
        target.hitPoint -= this.attackPower *(1- target.defencePower/100.0) * (Math.random()*0.4 + 0.8);
        System.out.println(String.format("%s's magic hit %s, dealing %.2f damage.",
                this.playerName, target.getPlayerName(), this.attackPower - target.getDefencePower()));
    }
}
