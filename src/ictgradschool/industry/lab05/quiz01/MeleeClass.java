package ictgradschool.industry.lab05.quiz01;

import ictgradschool.industry.lab05.quiz01.GameClass;

public class MeleeClass extends GameClass {

    public MeleeClass(String name) {
        super(name);
    }

    @Override
    public void attack(GameClass target) {
        target.hitPoint -= this.attackPower *(1- target.defencePower/100.0) * (Math.random()*0.2 + 0.9);
        System.out.println(String.format("%s pokes %s, dealing %.2f damage.",
                this.playerName, target.getPlayerName(), this.attackPower - target.getDefencePower()));
    }
}
