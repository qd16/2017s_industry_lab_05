package ictgradschool.industry.lab05.quiz01;

public class Warrior extends MeleeClass implements ITank, IDps {
    public Warrior(String name) {
        super(name);
        this.hitPoint = 1000.0;
        this.attackPower = 80.0;
        this.defencePower = 30.0;
    }


    @Override
    public void damageBoost() {
        System.out.println(String.format("%s shouts \"For the win!\".", this.playerName));
        this.attackPower *= 1.5;
    }

    @Override
    public void powerfulHit(GameClass target) {
        double damage = this.attackPower * 2  * (1 - target.defencePower/100.0) * (Math.random()*0.3 + 0.7);
        target.hitPoint -= damage;
        System.out.println(String.format("%s smashes his big blade to %s, deals %.2f damage.",
                this.playerName, target.playerName, damage));
    }

    @Override
    public void areaHit(GameClass[] targets) {
        double aoeDamage = this.attackPower * 0.8;
        for (GameClass target: targets) {
            target.hitPoint -= aoeDamage * (1 - target.defencePower/100.0);
        }
        System.out.println(String.format("%s waves bladestorm dealing %.2f near him.",
                this.playerName, aoeDamage));
    }

    @Override
    public void taunt(GameClass target) {
        System.out.println(String.format("%s taunted %s to attack him.",
                this.playerName, target.playerName));
    }

    @Override
    public void shieldWall() {
        if (this.defencePower < 50){
            this.defencePower += (100-defencePower) * 0.01;
        }
        System.out.println(String.format("%s raises his shield like a great wall.", this.playerName));
    }
}
