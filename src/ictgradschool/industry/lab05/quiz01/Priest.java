package ictgradschool.industry.lab05.quiz01;

public class Priest extends RangedClass implements IDps, IHealer {
    public Priest(String name) {
        super(name);
        this.hitPoint = 700.0;
        this.attackPower = 60.0;
        this.defencePower = 25.0;
    }


    @Override
    public void damageBoost() {
        this.attackPower *= 1.3;
        System.out.println(String.format("%s deeply meditates into his spiritual mind.", this.playerName));
    }

    @Override
    public void powerfulHit(GameClass target) {
        double damage = this.attackPower * 2 * (1 - target.defencePower/100.0) * (Math.random()*0.2 + 0.7);
        System.out.println(String.format("%s shoot a strong beam of holy light to %s, dealing %.2f damage.",
                this.playerName, target.playerName, damage));
    }

    @Override
    public void areaHit(GameClass[] targets) {
        double aoeDamage = this.attackPower * 0.8;
        for (GameClass target: targets) {
            target.hitPoint -= aoeDamage * (1 - target.defencePower/100.0);
        }
        System.out.println(String.format("%s prays for a holy sphere, dealing %.2f damage near him.",
                this.playerName, aoeDamage));
    }

    @Override
    public void rapidHeal(GameClass target) {
        double healAmount = this.attackPower;
        target.hitPoint += healAmount;
        System.out.println(String.format("%s fill %s with faithful light, healing %.2f health.",
                this.playerName, target.playerName, healAmount));
    }

    @Override
    public void groupHeal(GameClass[] targets) {
        double healAmount = this.attackPower * 0.5;
        for (GameClass target : targets) {
            target.hitPoint += healAmount;
        }
        System.out.println(String.format("%s granting his team with immense divine power, healing %.2f health.",
                this.playerName, healAmount));
    }
}
