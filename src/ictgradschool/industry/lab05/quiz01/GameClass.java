package ictgradschool.industry.lab05.quiz01;

/**
 * The base class of all game classes
 */
public abstract class GameClass {

    public GameClass(String name){
        this.playerName = name;
    }

    protected String playerName;
    protected double hitPoint;
    protected double attackPower;
    protected double defencePower;

    public boolean isAlive(){
        return this.hitPoint > 0;
    }

    protected abstract void attack(GameClass other);

    public String getPlayerName() {
        return playerName;
    }

    public double getAttackPower() {
        return attackPower;
    }

    public double getDefencePower() {
        return defencePower;
    }

    public double getHitPoint() {
        return hitPoint;
    }

}
