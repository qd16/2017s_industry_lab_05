package ictgradschool.industry.lab05.quiz01;

public interface IHealer {
    void rapidHeal(GameClass target);
    void groupHeal(GameClass[] targets);
}
