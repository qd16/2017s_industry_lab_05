package ictgradschool.industry.lab05.quiz01;

public class Mage extends RangedClass implements IDps {
    public Mage(String name) {
        super(name);
        this.hitPoint = 800.0;
        this.attackPower = 150.0;
        this.defencePower = 20.0;
    }


    @Override
    public void damageBoost() {
        this.attackPower *= 1.8;
        System.out.println(String.format("Arcane energy surged through %s as she drew deep from her own power."
                , this.playerName));
    }

    @Override
    public void powerfulHit(GameClass target) {
        double damage = this.attackPower * 2 * (1 - target.defencePower/100.0) * (Math.random()*0.6 + 0.8);
        System.out.println(String.format("%s casts a huge pyroblast to %s, dealing %.2f damage.",
                this.playerName, target.playerName, damage));
    }

    @Override
    public void areaHit(GameClass[] targets) {
        double aoeDamage = this.attackPower * 0.8;
        for (GameClass target: targets) {
            target.hitPoint -= aoeDamage * (1 - target.defencePower/100.0);
        }
        System.out.println(String.format("%s summons a frost nova dealing %.2f damage near her.",
                this.playerName, aoeDamage));
    }
}
