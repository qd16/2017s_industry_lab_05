package ictgradschool.industry.lab05.quiz01;

public interface IDps {
    void damageBoost();
    void powerfulHit(GameClass target);
    void areaHit(GameClass[] targets);
}
