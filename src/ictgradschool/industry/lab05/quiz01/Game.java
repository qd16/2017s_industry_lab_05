package ictgradschool.industry.lab05.quiz01;

import java.util.Arrays;
import java.util.Random;

public class Game {

    public static void main(String[] args) {

        start();
    }

    public static void start(){
        // TODO use List<GameClass>, for removing dead player out of the list
        GameClass[] players = new GameClass[9];
        players[0] = new Warrior("Blade Master");
        players[1] = new Mage("Archmage");
        players[2] = new Priest("Light Bringer");
        players[3] = new Warrior("Stone Heart");
        players[4] = new Mage("Ward Winter");
        players[5] = new Priest("Prince Joyce");
        players[6] = new Warrior("Fighter Tebbe");
        players[7] = new Mage("Obcasus");
        players[8] = new Priest("Regidana");

        Random rand = new Random();

        while (true){
            for (GameClass p : players) {
                if (p.isAlive()){
                    if (p instanceof ITank){
                        doSomething((ITank) p, players);
                    }
                    if (p instanceof IDps){
                        doSomething((IDps) p, players);
                        p.attack(players[rand.nextInt(9)]);
                    }
                    if (p instanceof IHealer){
                        doSomething((IHealer) p, players);
                    }
                } else{
                    System.out.println(String.format("%s is dead. ", p.getPlayerName()));
                }

            }
            // Break out the loop if there is only one player alive
            if (Arrays.stream(players).filter(GameClass::isAlive).count() <= 1)
                break;
        }
        System.out.println(Arrays.stream(players).filter(GameClass::isAlive).findFirst().orElse(new GameClass("Nobody") {
            @Override
            protected void attack(GameClass other) {

            }
        }).getPlayerName()
                + " is the WINNER!");
    }


    public static void doSomething(ITank player, GameClass[] players){
        Random rand = new Random();
        switch (rand.nextInt(2)){
            case 0:
                player.shieldWall();
                break;
            case 1:
                player.taunt(players[rand.nextInt(9)]);
                break;
        }
    }

    public static void doSomething(IDps player, GameClass[] players){
        Random rand = new Random();

        //TODO prevent player hit himself or dead player
        switch (rand.nextInt(4)){
            case 0:
                player.areaHit(new GameClass[]{
                        players[rand.nextInt(3)],
                        players[rand.nextInt(3) + 3],
                        players[rand.nextInt(3) + 6]
                });
                break;
            case 1:
                player.damageBoost();
                break;
            case 2:
                player.powerfulHit(players[rand.nextInt(9)]);
                break;
        }
    }

    public static void doSomething(IHealer player, GameClass[] players){
        Random rand = new Random();
        switch (rand.nextInt(2)){
            case 0:
                player.rapidHeal(players[rand.nextInt(9)]);
                break;
            case 1:
                player.groupHeal(new GameClass[]{
                        players[rand.nextInt(3)],
                        players[rand.nextInt(3) + 3],
                        players[rand.nextInt(3) + 6],
                });
                break;
        }
    }
}
