package ictgradschool.industry.lab05.ex03;

public class Test2 extends SuperClass {
    static int x = 15 ;
    static int y = 15 ;
    int x2 = 20 ;
    static int y2 = 20 ;
    Test2 () {
        x2 = y2 ++;
    }
    public int foo2 () {
        return x2 ;
    }
    public static int goo2 () {
        return y2 ;
    }
    public static int goo (){
        return y2 ;
    }
    public static void main ( String [] args ) {
        SuperClass s2 = new Test2 ();                              // the static type of s2 is SuperClass
        System.out.println(s2.getClass());
        System . out . println ( "\nThe static Binding" );
        System . out . println ( "S2.x = " + s2 . x );
        System . out . println ( "S2.y = " + s2 . y );
        System . out . println ( "S2.foo() = " + s2 . foo ());
        System . out . println ( "S2.goo2() = " + s2 . goo ());     //s2.goo() calls SuperClass goo()
        // We cannot make a call to s2.foo2() Because it can only call override versions of SuperClass methods
        Test1 t2 = (Test1) new SuperClass();    // Can compile, but will throw exception at run time.
                                                // Because cannot cast base class instance to a derived class instance.

    }
}
